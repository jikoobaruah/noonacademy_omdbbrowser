package com.jikoo.noonacademy.livedata;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.jikoo.noonacademy.App;


public class ConnectivityLiveData extends LiveData<Boolean> {

    private ConnectivityManager manager;
    private ConnectivityManager.NetworkCallback networkCallback;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ConnectivityLiveData() {
        manager = (ConnectivityManager) App.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                postValue(true);
            }

            @Override
            public void onLost(Network network) {
                postValue(false);
            }
        };
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActive() {
        super.onActive();
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
        postValue(activeNetwork.isConnectedOrConnecting() == true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            manager.registerDefaultNetworkCallback(networkCallback);
        } else {
            NetworkRequest.Builder builder = null;
            builder = new NetworkRequest.Builder();
            manager.registerNetworkCallback(builder.build(), networkCallback);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onInactive() {
        super.onInactive();
        manager.unregisterNetworkCallback(networkCallback);
    }
}
