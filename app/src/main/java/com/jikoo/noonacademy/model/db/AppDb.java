package com.jikoo.noonacademy.model.db;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.jikoo.noonacademy.model.db.dao.BookMarkDao;
import com.jikoo.noonacademy.model.dto.BookMark;

@Database(entities = {BookMark.class}, version = 1)
public abstract class AppDb extends RoomDatabase {
    public abstract BookMarkDao bookMarkDao();
}