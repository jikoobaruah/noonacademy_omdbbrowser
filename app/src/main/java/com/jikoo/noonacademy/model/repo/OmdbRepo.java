package com.jikoo.noonacademy.model.repo;

import android.arch.lifecycle.MutableLiveData;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.jikoo.noonacademy.model.dto.SearchResponse;
import com.jikoo.noonacademy.model.dto.SearchResult;
import com.jikoo.noonacademy.model.network.OmdbService;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OmdbRepo {

    private static final String TAG = "OmdbRepo";

    private String currentSearch;
    private int page ;
    private Call<SearchResponse> currentCall;

    private MutableLiveData<SearchResult> searchResultLiveData;


    private OmdbService omdbService;

    @Inject
    public OmdbRepo(OmdbService omdbService1){
        omdbService = omdbService1;
        searchResultLiveData = new MutableLiveData<>();
    }

    public void search(String currentSearchString, int page) {

        Log.d(TAG, "search > " + currentSearchString);

        if (currentSearchString.equals(currentSearch) && this.page == page) {
            Log.d(TAG, "search already running> " + currentSearchString);
            return;
        }
        if (currentCall != null)
            currentCall.cancel();

        currentSearch = currentSearchString;
        this.page = page;
        currentCall = omdbService.search(currentSearch,page);
        Log.d(TAG, "enqueue request> " + currentSearch);
        post(SearchResult.getFetching(currentSearch));
        currentCall.enqueue(new SearchResponseCallback(currentSearch));

    }



    public MutableLiveData<SearchResult> getLiveData() {
        return searchResultLiveData;
    }




    class SearchResponseCallback implements Callback<SearchResponse>{


        private final String currentQuery;

        public SearchResponseCallback(String currentQuery) {
            this.currentQuery = currentQuery;
        }

        @Override
        public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
            if (response.isSuccessful()){
                Log.d(TAG,"success request> "+ call.request().url() );
                Log.d(TAG,"success response> "+ response.body().toString() );
                if (response.body()!= null && !TextUtils.isEmpty(response.body().getError())){
                    post(SearchResult.getFetchingFail(currentQuery,response.body().getError()));
                }else{
                    post(SearchResult.getFetchingSuccess(currentQuery,response.body()));
                }

            }else{
                handleError(call, response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<SearchResponse> call, Throwable t) {
            if (call.isCanceled()) {
                Log.d(TAG, "cancelled >> " + call.request().url());
                post(SearchResult.getFetchingCancelled(currentQuery));
            } else {
                Log.d(TAG, "failed >> " + call.request().url() + " :: "+t.getMessage());
                post(SearchResult.getFetchingFail(currentQuery,t.getMessage()));
            }

        }

        private void handleError(Call<SearchResponse> call, ResponseBody errorBody) {
            try {
                SearchResponse response = new Gson().fromJson(errorBody.string(), SearchResponse.class);
                post(SearchResult.getFetchingFail(currentQuery,response.getError()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void post(SearchResult result) {
        searchResultLiveData.postValue(result);

    }


}
