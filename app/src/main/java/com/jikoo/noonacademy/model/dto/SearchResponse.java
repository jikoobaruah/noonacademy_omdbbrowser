package com.jikoo.noonacademy.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchResponse {

    @SerializedName("Response")
    private String response;

    @SerializedName("Error")
    private String error;

    @SerializedName("totalResults")
    private String totalResults;

    @SerializedName("Search")
    private ArrayList<SearchItem> items;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public ArrayList<SearchItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<SearchItem> items) {
        this.items = items;
    }

    public static class SearchItem  implements Parcelable {
        @SerializedName("Title")
        private String title;

        @SerializedName("Year")
        private String year;

        @SerializedName("imdbID")
        private String imdbId;

        @SerializedName("Type")
        private String type;

        @SerializedName("Poster")
        private String poster;


        protected SearchItem(Parcel in) {
            title = in.readString();
            year = in.readString();
            imdbId = in.readString();
            type = in.readString();
            poster = in.readString();
        }

        public static final Creator<SearchItem> CREATOR = new Creator<SearchItem>() {
            @Override
            public SearchItem createFromParcel(Parcel in) {
                return new SearchItem(in);
            }

            @Override
            public SearchItem[] newArray(int size) {
                return new SearchItem[size];
            }
        };

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getImdbId() {
            return imdbId;
        }

        public void setImdbId(String imdbId) {
            this.imdbId = imdbId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPoster() {
            return poster;
        }

        public void setPoster(String poster) {
            this.poster = poster;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(title);
            dest.writeString(year);
            dest.writeString(imdbId);
            dest.writeString(type);
            dest.writeString(poster);
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
