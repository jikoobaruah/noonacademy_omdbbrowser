package com.jikoo.noonacademy.model.dto;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(indices = {@Index(value ={"showId"},unique = true)})
public class BookMark {

    @PrimaryKey(autoGenerate = true)
    private long id;

    private String showId;


    private String image;


    private String name;

    private int createDateSecs = (int) (System.currentTimeMillis()/1000);

    public BookMark(String showId, String image, String name) {
        this.showId = showId;
        this.image = image;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getShowId() {
        return showId;
    }

    public void setShowId(String showId) {
        this.showId = showId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCreateDateSecs() {
        return createDateSecs;
    }

    public void setCreateDateSecs(int createDateSecs) {
        this.createDateSecs = createDateSecs;
    }
}
