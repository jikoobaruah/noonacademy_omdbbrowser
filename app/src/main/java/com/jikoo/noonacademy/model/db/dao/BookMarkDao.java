package com.jikoo.noonacademy.model.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;


import com.jikoo.noonacademy.model.dto.BookMark;

import java.util.List;

@Dao
public interface BookMarkDao {

    @Query("SELECT * FROM BookMark")
    LiveData<List<BookMark>> getAll();


//    @Query("DELETE FROM venuelikes WHERE venueId = :venueId")
//    void delete(String venueId);

    @Delete
    void delete(BookMark bookMark);

    @Insert
    void insert(BookMark bookMark);

    @Query("SELECT * FROM BookMark WHERE showId = :showId")
    BookMark getByVenueId(String showId);
}
