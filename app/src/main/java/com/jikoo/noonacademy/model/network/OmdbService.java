package com.jikoo.noonacademy.model.network;


import com.jikoo.noonacademy.model.dto.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public interface OmdbService {

    @GET(".")
    Call<SearchResponse> search(@Query("s") String currentSearch, @Query("page") int page);





}
