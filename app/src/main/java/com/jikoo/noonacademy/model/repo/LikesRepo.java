package com.jikoo.noonacademy.model.repo;

import android.arch.lifecycle.LiveData;


import com.jikoo.noonacademy.model.db.AppDb;
import com.jikoo.noonacademy.model.dto.BookMark;
import com.jikoo.noonacademy.model.dto.SearchResponse;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

public class LikesRepo {


    private final AppDb appDb;

    private static ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Inject
    public LikesRepo(AppDb appDb) {
        this.appDb = appDb;
    }


    public LiveData<List<BookMark>> fetchLikes() {
        return appDb.bookMarkDao().getAll();
    }

    public void onLikeChanged(final SearchResponse.SearchItem item, final boolean isChecked) {

        executorService.submit(new Runnable() {
            @Override
            public void run() {
                if (isChecked) {
                    appDb.bookMarkDao().insert(new BookMark(item.getImdbId(),item.getPoster(),item.getTitle()));
                } else {
                    BookMark bookMark = appDb.bookMarkDao().getByVenueId(item.getImdbId());
                    appDb.bookMarkDao().delete(bookMark);
                }
            }
        });


    }
}
