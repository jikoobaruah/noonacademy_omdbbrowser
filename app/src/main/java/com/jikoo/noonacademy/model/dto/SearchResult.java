package com.jikoo.noonacademy.model.dto;

public class SearchResult {

    private SearchResult(){}

    private SearchResult(STATE idle) {
        state = idle;
    }

    private SearchResult(STATE fetching, String query) {
        searchQuery = query;
        state = fetching;
    }

    private SearchResult(STATE fetchSuccess, String query, SearchResponse response) {
        searchQuery = query;
        state = fetchSuccess;
        searchResponse = response;
    }

    private SearchResult(STATE fetchFail, String query, String errorString) {
        searchQuery = query;
        state = fetchFail;
        error = errorString;
    }


    public enum STATE {
        IDLE, FETCHING, FETCH_SUCCESS, FETCH_FAIL, FETCH_CANCEL
    }

    private STATE state;

    private String searchQuery;

    private String error;

    private SearchResponse searchResponse;

    public STATE getState() {
        return state;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public String getError() {
        return error;
    }

    public SearchResponse getSearchResponse() {
        return searchResponse;
    }

    public static SearchResult getIdle(){
        return new SearchResult(STATE.IDLE);
    }

    public static SearchResult getFetching(String query){
        return new SearchResult(STATE.FETCHING,query);
    }

    public static SearchResult getFetchingSuccess(String query, SearchResponse response){
        return new SearchResult(STATE.FETCH_SUCCESS,query,response);
    }

    public static SearchResult getFetchingFail(String query, String error){
        return new SearchResult(STATE.FETCH_FAIL,query,error);
    }

    public static SearchResult getFetchingCancelled(String currentSearch) {
       return new SearchResult(STATE.FETCH_CANCEL,currentSearch);
    }

}
