package com.jikoo.noonacademy.viewmodel;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;


import com.jikoo.noonacademy.model.dto.BookMark;
import com.jikoo.noonacademy.model.dto.SearchResponse;
import com.jikoo.noonacademy.model.dto.SearchResult;
import com.jikoo.noonacademy.model.repo.LikesRepo;
import com.jikoo.noonacademy.model.repo.OmdbRepo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class OmdbViewModel extends BaseViewModel {

    private OmdbRepo mOmdbRepo;

    private LikesRepo likesRepo;

    private String currentString;

    private int page;

    @Inject
    public OmdbViewModel(@NonNull Application application, OmdbRepo omdbRepo, LikesRepo likesRepo) {
        super(application);
        this.mOmdbRepo = omdbRepo;
        this.likesRepo = likesRepo;
    }

    public void fetch(String currentSearchString) {
        currentString = currentSearchString;
        page = 1;
        mOmdbRepo.search(currentString,page);
    }

    public MutableLiveData<SearchResult> getLiveData() {
        return mOmdbRepo.getLiveData();
    }

    public void onLikeChanged(SearchResponse.SearchItem item, boolean isChecked) {
        likesRepo.onLikeChanged( item,  isChecked);
    }

    public LiveData<List<String>> fetchLikes() {
        return Transformations.map(likesRepo.fetchLikes(), new Function<List<BookMark>, List<String>>() {
            @Override
            public List<String> apply(List<BookMark> input) {
                if (input == null) return null;
                List<String> likeIds = new ArrayList<>();
                for (BookMark vl : input){
                    likeIds.add(vl.getShowId());
                }
                return likeIds;
            }
        });
    }


    public LiveData<List<BookMark>> fetchLikesList() {
        return likesRepo.fetchLikes();
    }


    public void fetchMore() {
        page++;
        mOmdbRepo.search(currentString,page);

    }

    public int getPage() {
        return page;
    }
}
