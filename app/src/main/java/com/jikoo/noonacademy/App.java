package com.jikoo.noonacademy;

import android.content.Context;

import com.facebook.stetho.Stetho;
import com.jikoo.noonacademy.di.ApplicationComponent;
import com.jikoo.noonacademy.di.DaggerApplicationComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class App extends DaggerApplication {
    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;

        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this);

    }

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
        component.inject(this);

        return component;
    }
}
