package com.jikoo.noonacademy.di;

import android.arch.persistence.room.Room;

import com.jikoo.noonacademy.App;
import com.jikoo.noonacademy.BuildConfig;
import com.jikoo.noonacademy.model.db.AppDb;
import com.jikoo.noonacademy.model.network.OmdbService;

import java.io.File;
import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
@Module(includes = ViewModelModule.class)
public class ApplicationModule {

    private static final String BASE_URL = "https://www.omdbapi.com/";

    @Singleton
    @Provides
    static Retrofit provideRetrofit() {
        File httpCacheDirectory = new File(App.getAppContext().getCacheDir(), "responses");
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(httpCacheDirectory, cacheSize);

        OkHttpClient.Builder builder = new OkHttpClient.Builder().cache(cache);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();

                HttpUrl url = request.url().newBuilder().
                        addQueryParameter("apikey", BuildConfig.API_KEY).
                        build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        });

        return new Retrofit.Builder().baseUrl(BASE_URL).client(builder.build()).addConverterFactory(GsonConverterFactory.create()).build();

    }

    @Singleton
    @Provides
    static OmdbService provideRetrofitService(Retrofit retrofit) {
        return retrofit.create(OmdbService.class);
    }



    @Singleton
    @Provides
    static AppDb provideDb() {
        return Room.databaseBuilder(App.getAppContext(), AppDb.class, "app.db").build();
    }
}
