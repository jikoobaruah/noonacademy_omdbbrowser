package com.jikoo.noonacademy.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.jikoo.noonacademy.R;
import com.jikoo.noonacademy.databinding.ActivityMainBinding;
import com.jikoo.noonacademy.di.ViewModelFactory;
import com.jikoo.noonacademy.model.dto.BookMark;
import com.jikoo.noonacademy.model.dto.SearchResponse;
import com.jikoo.noonacademy.model.dto.SearchResult;
import com.jikoo.noonacademy.utils.Utils;
import com.jikoo.noonacademy.view.common.BaseActivity;
import com.jikoo.noonacademy.view.common.widget.HorizontalSpaceItemDecoration;
import com.jikoo.noonacademy.view.common.widget.SearchEditText;
import com.jikoo.noonacademy.view.common.widget.VerticalSpaceItemDecoration;
import com.jikoo.noonacademy.viewmodel.OmdbViewModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;


public class MainActivity extends BaseActivity implements OmdbListAdapter.Callback {

    private static final int TRIGGER_AUTO_COMPLETE = 100;
    private static final long AUTO_COMPLETE_DELAY = 300;
    private static final int THRESHOLD = 2;

    private ActivityMainBinding binding;

    @Inject
    ViewModelFactory viewModelFactory;

    private OmdbViewModel viewModel;
    private String currentSearchString;
    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == TRIGGER_AUTO_COMPLETE) {
                if (!TextUtils.isEmpty(binding.etSearchInput.getText())) {
                    if (!TextUtils.isEmpty(currentSearchString) && currentSearchString.equals(binding.etSearchInput.getText().toString().trim())) return false;
                    currentSearchString = binding.etSearchInput.getText().toString().trim();
                    viewModel.fetch(currentSearchString);
                    loading = true;
                }
            }
            return false;
        }
    });


    private Observer<SearchResult> searchResultObserver = new Observer<SearchResult>() {
        @Override
        public void onChanged(@Nullable SearchResult searchResult) {
            loading = false;
            switch (searchResult.getState()) {
                case IDLE:
                    break;
                case FETCHING:
                    break;
                case FETCH_FAIL:
                    showError(searchResult);
                    break;
                case FETCH_SUCCESS:
                    showSuccess(searchResult);
                    break;
            }
        }
    };
    private Observer<List<String>> likesObserver = new Observer<List<String>>() {
        @Override
        public void onChanged(@Nullable List<String> strings) {
            mOmdbListAdapter.setLikesSet(new HashSet<String>(strings));
        }
    };

    private Observer<List<BookMark>> likesListObserver = new Observer<List<BookMark>>() {
        @Override
        public void onChanged(@Nullable List<BookMark> bookMarks) {
            if (bookMarks == null || bookMarks.size() == 0) {
                binding.rvBookmarks.setVisibility(View.GONE);
            } else {
                binding.rvBookmarks.setVisibility(View.VISIBLE);
            }

            mOmdbBookMarkAdapter.setLikes(bookMarks);
        }
    };
    private BookMarkAdapter mOmdbBookMarkAdapter;
    private LinearLayoutManager mListLayoutManager;
    private boolean loading;

    private void showError(SearchResult searchResult) {
        mOmdbListAdapter.clear();
    }


    private Drawable drawableLeft;
    private Drawable drawableRight;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == 0) {
                binding.etSearchInput.setCompoundDrawables(drawableLeft, null, null, null);
            } else {
                binding.etSearchInput.setCompoundDrawables(drawableLeft, null, drawableRight, null);
            }

            if (s.length() < THRESHOLD) {
                return;
            }
            handler.removeMessages(TRIGGER_AUTO_COMPLETE);
            handler.sendEmptyMessageDelayed(TRIGGER_AUTO_COMPLETE, AUTO_COMPLETE_DELAY);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private SearchEditText.DrawableClickListener searchDrawableClickListener = new SearchEditText.DrawableClickListener() {
        @Override
        public void onClick(DrawablePosition target) {
            if (target == DrawablePosition.RIGHT) {
                binding.etSearchInput.removeTextChangedListener(textWatcher);
                binding.etSearchInput.setText("");
                binding.etSearchInput.addTextChangedListener(textWatcher);
                binding.etSearchInput.requestFocus();
            }
        }
    };
    private OmdbListAdapter mOmdbListAdapter;


    private void showSuccess(SearchResult searchResult) {
        if (!searchResult.getSearchQuery().equals(currentSearchString)) return;
        mOmdbListAdapter.updateList(searchResult.getSearchResponse().getItems(),currentSearchString,viewModel.getPage());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.toolbar);

        binding.etSearchInput.addTextChangedListener(textWatcher);
        drawableLeft = binding.etSearchInput.getCompoundDrawables()[0];
        drawableRight = binding.etSearchInput.getCompoundDrawables()[2];
        binding.etSearchInput.setCompoundDrawables(drawableLeft, null, null, null);
        binding.etSearchInput.setDrawableClickListener(searchDrawableClickListener);
        binding.etSearchInput.requestFocus();
        binding.etSearchInput.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showKeyboard(binding.etSearchInput);
            }
        }, 300);


        mOmdbListAdapter = new OmdbListAdapter(this);
        binding.rv.setAdapter(mOmdbListAdapter);
        mListLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.rv.setLayoutManager(mListLayoutManager);
        binding.rv.addItemDecoration(new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.activity_vertical_margin)));

        binding.rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int totalItemCount = mListLayoutManager.getItemCount();
                int lastVisibleItem = mListLayoutManager.findLastVisibleItemPosition();
                int visibleThreshold = 3;
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    viewModel.fetchMore();
                    loading = true;
                }
            }
        });

        if (savedInstanceState != null) {
            currentSearchString = savedInstanceState.getString(OUT_STATE.QUERY);
        }

        mOmdbBookMarkAdapter = new BookMarkAdapter();
        binding.rvBookmarks.setAdapter(mOmdbBookMarkAdapter);
        binding.rvBookmarks.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvBookmarks.addItemDecoration(new HorizontalSpaceItemDecoration((int) getResources().getDimension(R.dimen.activity_vertical_margin)));


        viewModel = ViewModelProviders.of(this, viewModelFactory).get(OmdbViewModel.class);
        viewModel.getLiveData().observe(this, searchResultObserver);
        viewModel.fetchLikes().observe(this, likesObserver);
        viewModel.fetchLikesList().observe(this, likesListObserver);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(OUT_STATE.QUERY, currentSearchString);
        outState.putParcelableArrayList(OUT_STATE.LIST, new ArrayList<Parcelable>(mOmdbListAdapter.getList()));
    }

    @Override
    public void onCheckChanged(SearchResponse.SearchItem item, boolean isChecked) {
        viewModel.onLikeChanged(item, isChecked);
    }

    interface OUT_STATE {
        String QUERY = "q";
        String LIST = "l";
    }
}
