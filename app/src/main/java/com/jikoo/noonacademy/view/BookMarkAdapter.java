package com.jikoo.noonacademy.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.jikoo.noonacademy.R;
import com.jikoo.noonacademy.databinding.ItemBookmarkBinding;
import com.jikoo.noonacademy.model.dto.BookMark;

import java.util.ArrayList;
import java.util.List;

public class BookMarkAdapter extends RecyclerView.Adapter<BookMarkAdapter.BookMarkHolder> {

    private List<BookMark> mBookMarks;

    public BookMarkAdapter(){
        mBookMarks = new ArrayList<>();
    }

    @NonNull
    @Override
    public BookMarkHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemBookmarkBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_bookmark, viewGroup,false);
        return new BookMarkHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BookMarkHolder bookMarkHolder, int i) {
        bookMarkHolder.bindView();
    }

    @Override
    public int getItemCount() {
        return mBookMarks.size();
    }

    public void setLikes(List<BookMark> bookMarks) {
        if (bookMarks == null) {
            int size = getItemCount();
            mBookMarks.clear();
            notifyItemRangeRemoved(0,size);
            return;
        }

        final BookMarkDiffCallback diffCallback = new BookMarkDiffCallback(mBookMarks, bookMarks);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        mBookMarks.clear();
        mBookMarks.addAll(bookMarks);
        diffResult.dispatchUpdatesTo(this);
    }

    public class BookMarkHolder extends RecyclerView.ViewHolder{

        private ItemBookmarkBinding mBinding;

        public BookMarkHolder(ItemBookmarkBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void bindView() {
            mBinding.setBookmark(mBookMarks.get(getAdapterPosition()));
            mBinding.executePendingBindings();
            Glide.with(mBinding.iv).load(mBookMarks.get(getAdapterPosition()).getImage()).into(mBinding.iv);

        }
    }
}
