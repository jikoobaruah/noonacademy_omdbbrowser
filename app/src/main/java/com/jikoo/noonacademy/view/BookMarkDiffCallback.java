package com.jikoo.noonacademy.view;

import android.support.v7.util.DiffUtil;

import com.jikoo.noonacademy.model.dto.BookMark;
import com.jikoo.noonacademy.model.dto.SearchResponse;

import java.util.List;

public class BookMarkDiffCallback extends DiffUtil.Callback {

    private final List<BookMark> oldList;
    private final List<BookMark> newList;

    public BookMarkDiffCallback(List<BookMark> oldVenusList, List<BookMark> newSearchResponse) {
        this.oldList = oldVenusList;
        this.newList = newSearchResponse;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getShowId().equals(newList.get(newItemPosition).getShowId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final BookMark oldItem = oldList.get(oldItemPosition);
        final BookMark newItem = newList.get(newItemPosition);

        return oldItem.getShowId().equals(newItem.getShowId());
    }

    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}