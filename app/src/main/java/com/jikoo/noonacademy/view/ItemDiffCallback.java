package com.jikoo.noonacademy.view;

import android.support.v7.util.DiffUtil;

import com.jikoo.noonacademy.model.dto.SearchResponse;

import java.util.List;

public class ItemDiffCallback extends DiffUtil.Callback {

    private final List<SearchResponse.SearchItem> oldList;
    private final List<SearchResponse.SearchItem> newList;

    public ItemDiffCallback(List<SearchResponse.SearchItem> oldVenusList, List<SearchResponse.SearchItem> newSearchResponse) {
        this.oldList = oldVenusList;
        this.newList = newSearchResponse;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getImdbId().equals(newList.get(newItemPosition).getImdbId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final SearchResponse.SearchItem oldItem = oldList.get(oldItemPosition);
        final SearchResponse.SearchItem newItem = newList.get(newItemPosition);

        return oldItem.getImdbId().equals(newItem.getImdbId());
    }

    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}