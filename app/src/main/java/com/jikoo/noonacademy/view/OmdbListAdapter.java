package com.jikoo.noonacademy.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.bumptech.glide.Glide;
import com.jikoo.noonacademy.R;
import com.jikoo.noonacademy.databinding.ItemVenueBinding;
import com.jikoo.noonacademy.model.dto.SearchResponse;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OmdbListAdapter extends RecyclerView.Adapter<OmdbListAdapter.VenuesHolder> {

    private final Callback callback;
    private List<SearchResponse.SearchItem> mItems;
    private Set<String> likeSet;

    private String searchString;
    private int page = 0;

    public OmdbListAdapter(Callback callback){
        this.callback = callback;
        mItems = new ArrayList<>();
        likeSet = new HashSet<>();
    }

    @NonNull
    @Override
    public VenuesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemVenueBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_venue,viewGroup,false);
        return new VenuesHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VenuesHolder venuesHolder, int i) {
        venuesHolder.bindView();
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setLikesSet(Set<String> likes){


        if (likes != null)
            likeSet.removeAll(likes);

        Set<String> toUnlike = new HashSet<>(likeSet);

        likeSet.clear();

        if (likes != null)
            likeSet = likes;

        String id;
        for (int i =0 ; i< mItems.size() ; i++){
            id = mItems.get(i).getImdbId();
            if (likeSet.contains(id) || toUnlike.contains(id))
                notifyItemChanged(i);
        }
    }

    public void updateList(List<SearchResponse.SearchItem> newList , String query, int page){
        if (newList == null)
            return;

        if (query.equals(searchString) && this.page != page){
            this.page = page;
            int start = getItemCount();
            int size = newList.size();
            mItems.addAll(newList);
            notifyItemRangeInserted(start,size);
        }else{
            searchString = query;
            final ItemDiffCallback diffCallback = new ItemDiffCallback(mItems, newList);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

            this.mItems.clear();
            this.mItems.addAll(newList);
            diffResult.dispatchUpdatesTo(this);
        }


    }

    public void clear() {
        int size = mItems.size();
        mItems.clear();
        notifyItemRangeRemoved(0,size);
    }

    public List<SearchResponse.SearchItem> getList() {
        return mItems;
    }

    public Set<String> getLikesSet() {
        return likeSet;
    }

    public class VenuesHolder extends RecyclerView.ViewHolder{

        private ItemVenueBinding binding;

        public VenuesHolder(@NonNull ItemVenueBinding itemVenueBinding) {
            super(itemVenueBinding.getRoot());
            binding = itemVenueBinding;


            binding.cbLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    callback.onCheckChanged(mItems.get(getAdapterPosition()),isChecked);
                    if (isChecked)
                        likeSet.add(mItems.get(getAdapterPosition()).getImdbId());
                    else
                        likeSet.remove(mItems.get(getAdapterPosition()).getImdbId());

                }
            });
        }

        public void bindView() {
            SearchResponse.SearchItem item = mItems.get(getAdapterPosition());
            binding.setItem(item);
            binding.setIsLiked(likeSet.contains(item.getImdbId()));
            binding.executePendingBindings();

            Glide.with(binding.ivIcon).load(item.getPoster()).into(binding.ivIcon);


        }
    }

    interface Callback {

        void onCheckChanged(SearchResponse.SearchItem item, boolean isChecked);
    }
}
